import csv
import io
import rawdata2actions
import pandas
import pickle
import os
import sklearn
import tempfile

os.tmpdir = tempfile.gettempdir

def preprocess_batch(batch):
    filename = os.path.join(os.tmpdir(), 'tmp.csv')
    left_state = False
    with open(filename, 'w') as csvfile:
        spamwriter = csv.writer(csvfile)
        spamwriter.writerow(['record timestamp','client timestamp','button','state','x','y'])
        for item in batch:
            if item['button'] == 'Left' and item['state'] == 'Pressed':
                left_state = True
            elif item['button'] == 'Left' and item['state'] == 'Released':
                left_state = False
            elif left_state and item['button'] == 'NoButton':
                item['state'] = 'Drag'
            spamwriter.writerow([item['t'], item['t'], item['button'], item['state'], item['x'], item['y']])
    actions = open(os.path.join(os.tmpdir(), 'actions.txt'), 'w')
    rawdata2actions.processSession1(filename, actions)
    actions.close()
    df = pandas.read_csv(os.path.join(os.tmpdir(), 'actions.txt'))
    print(df)
    return df

def get_model(uid=None):
    if 'models' in os.listdir('.'):
        with open(f'models/{uid}.pkl', 'rb') as file:
            return pickle.load(file)
    else:
        with open(f'model.pkl', 'rb') as file:
            return pickle.load(file)

def process_batch(batch, uid):
    df = preprocess_batch(batch)
    # df.to_csv('data/dataframe.csv')
    model = get_model(uid)
    predictions = model.predict_proba(df)[:, 1]
    print(predictions)

    return {'pred': float(predictions.mean())}
