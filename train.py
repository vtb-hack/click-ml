from sklearn.ensemble import RandomForestClassifier
from datasets import LegacyDataset, LegacySessionDataset
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, f1_score, roc_auc_score, roc_curve
import scikitplot as skplt
import pickle
import numpy
import os
from matplotlib import pyplot

metrics = {
    'session': {
        'acc': [],
        'f1': [],
        'roc_auc': [],
    },
    'action': {
        'acc': [],
        'f1': [],
        'roc_auc': [],
        'tn': [],
        'thr': []
    }
}

action_probas = None
action_truth = []
session_probas = []
session_truth = []

def pretty(d, indent=0):
   for key, value in d.items():
      print('\t' * indent, key)
      if isinstance(value, dict):
         pretty(value, indent+1)
      elif isinstance(value, list):
         elem = '\t' * (indent+1)
         if len(value) == 0:
            print(elem, 'nope')
            return
         elem += f'{sum(value)/len(value):5.4f}: '
         for i in range(len(value)):
             elem += f'{value[i]:5.4f} '
         print(elem)
      else:
         print('\t' * (indent+1) + str(value))

class FalseNegativeRate:
    def __init__(self, max_false_positive=0.99):
        self.mfpos = max_false_positive

    def __call__(self, truth, predicted):
        L = 0
        R = 1.0
        for _ in range(30):
            thr = (L + R) / 2
            total_positive = 0
            false_negative = 0

            for i in range(predicted.shape[0]):
                total_positive += truth[i]
                false_negative += (predicted[i][1] < thr and truth[i] == 1)


            if false_negative / total_positive < (1 - self.mfpos):
                L = thr
            else:
                R = thr
        thr = R
        print(thr, false_negative, total_positive, false_negative / total_positive)
        print(f'THR {thr:5.4f}')

        total_negative = 0
        false_positive = 0

        for i in range(predicted.shape[0]):
            total_negative += 1 - truth[i]
            false_positive += predicted[i][1] >= thr and truth[i] == 0

        return false_positive / total_negative, thr


def train(data, val_data=None):
    global action_truth, action_probas
    X_train = [el[:-1] for el in data]
    Y_train = [el[-1] for el in data]

    clf = RandomForestClassifier(random_state=0, n_jobs=-1, n_estimators=1000)
    clf.fit(X_train, Y_train)

    if val_data is not None:
        X_test = [el[:-1] for el in val_data]
        Y_test = [el[-1] for el in val_data]
        predictions_proba = clf.predict_proba(X_test)
        predictions = clf.predict(X_test)
        acc = accuracy_score(Y_test, predictions)
        f1 = f1_score(Y_test, predictions)
        roc_auc = roc_auc_score(Y_test, [el[1] for el in predictions_proba]) # !!!
        print(f'Acc {acc:4.5f}')
        print(f'F1 {f1:4.5f}')
        print(f'ROC AUC {roc_auc:4.5f}')
        rate, thr = FalseNegativeRate()(Y_test, predictions_proba)
        tn = 1 - rate
        print(f'acceptable fraud detection rate {tn:4.5f}')
        metrics['action']['acc'].append(acc)
        metrics['action']['f1'].append(f1)
        metrics['action']['roc_auc'].append(roc_auc)
        metrics['action']['tn'].append(tn)
        metrics['action']['thr'].append(thr)
        action_truth += Y_test
        #print(action_probas)
        #print(predictions_proba[::, 1])
        if action_probas is None:
            action_probas = predictions_proba[::, 1]
        else:
            print('@', predictions_proba.shape, action_probas.shape)
            action_probas = numpy.concatenate((action_probas, predictions_proba[::, 1])) # [::, 1]
            print('B', predictions_proba.shape, action_probas.shape, len(action_truth))
        # print(clf.feature_importances_)

    return clf

def test_model_on_session(model, session, thr=0.5):
    global session_truth, session_probas
    truth = session[1]
    prediction = model.predict_proba(session[0])
    prediction = sum([el[1] for el in prediction]) / prediction.shape[0]
    session_probas.append(prediction)
    session_truth.append(session[1])
    return prediction >= thr

    if prediction >= thr and truth == 1:
        return 1
    elif prediction < thr and truth == 0:
        return 1
    else:
        return 0

def legacy_train(uid=7, no_known_frauds=True):
    if f"{uid}.pkl" not in os.listdir('preprocessed'):
        dataset = LegacyDataset(uid)
        with open(f'preprocessed/{uid}.pkl', 'wb') as file:
            pickle.dump(dataset, file)
    else:
        with open(f'preprocessed/{uid}.pkl', 'rb') as file:
            dataset = pickle.load(file)

    if f"{uid}-test.pkl" not in os.listdir('preprocessed'):
        dataset_test = LegacyDataset(uid, False)
        with open(f'preprocessed/{uid}-test.pkl', 'wb') as file:
            pickle.dump(dataset_test, file)
    else:
        with open(f'preprocessed/{uid}-test.pkl', 'rb') as file:
            dataset_test = pickle.load(file)

    if no_known_frauds:
        model = train(dataset.tolist(), dataset_test.tolist())
    else:
        train_data, test_data = train_test_split(dataset.tolist(), test_size=0.33)
        model = train(train_data, test_data)

    test_data = dataset_test.tolist()
    print('SESSION TEST')
    rate, thr = FalseNegativeRate()([el[-1] for el in test_data], model.predict_proba([el[:-1] for el in test_data]))

    if f"{uid}-session.pkl" not in os.listdir('preprocessed'):
        dataset_session = LegacySessionDataset(uid)
        with open(f'preprocessed/{uid}-session.pkl', 'wb') as file:
            pickle.dump(dataset_session, file)
    else:
        with open(f'preprocessed/{uid}-session.pkl', 'rb') as file:
            dataset_session = pickle.load(file)

    session_data = dataset_session.tolist()

    truth = []
    predicted = []
    for el in session_data:
        truth.append(el[1])
        predicted.append(test_model_on_session(model, el))

    acc = accuracy_score(truth, predicted)
    f1 = f1_score(truth, predicted)
    print(f"acc: {acc:5.4f}")
    print(f"f1 : {f1:5.4f}")
    metrics['session']['acc'].append(acc)
    metrics['session']['f1'].append(f1)
    print()
    return model

def legacy_test():
    # 9
    uids = [7, 15] #, 12, 15, 16, 20, 21, 23, 29, 35]
    for uid in uids:
        model = legacy_train(uid)
        with open(f'models/{uid}.pkl', 'wb') as file:
            pickle.dump(model, file)


if __name__ == "__main__":
    legacy_test()
    pretty(metrics)

    action_truth = numpy.array(action_truth)
    action_probas = action_probas.reshape((-1, 1))
    print(action_truth.shape, action_probas.shape)
    lr_fpr, lr_tpr, _ = roc_curve(action_truth, action_probas)
    pyplot.plot(lr_fpr, lr_tpr, marker='.', label='Random Forest Classsifier')
    # skplt.metrics.plot_roc(action_truth, action_probas, classes_to_plot=[0])
    pyplot.xlabel('False Positive Rate')
    pyplot.ylabel('True Positive Rate')
    pyplot.title('ROC curve')
    # show the legend
    pyplot.legend()
    # show the plot
    pyplot.show()
    pyplot.savefig('action_roc.png')


    session_probas = numpy.array(session_probas).reshape((-1, 1))
    lr_fpr, lr_tpr, _ = roc_curve(session_truth, session_probas)
    pyplot.plot(lr_fpr, lr_tpr, marker='.', label='Random Forest Classsifier')
    # skplt.metrics.plot_roc(action_truth, action_probas, classes_to_plot=[0])
    pyplot.xlabel('False Positive Rate')
    pyplot.ylabel('True Positive Rate')
    pyplot.title('ROC curve')
    # show the legend
    pyplot.legend()
    # show the plot
    pyplot.show()
    pyplot.savefig('session_roc.png')
