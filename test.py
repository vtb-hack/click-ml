import requests
import random
random.seed(1)
import json
'''
['t'], item['t'], item['button'], item['state'], item['x'], item['y']
'''

with open('data/myevents.txt') as file:
    data = [eval(line.rstrip()) for line in file.readlines()]
t = data[0]['t']
for i in range(len(data)):
    data[i]['t'] -= t
    data[i]['t'] /= 1000
#print(json.dumps({'batch': data}))
#exit(0)
# https://us-central1-majestic-disk-281618.cloudfunctions.net/batch
# http://localhost:8080
# 'https://us-central1-majestic-disk-281618.cloudfunctions.net/batch'
# 'http://localhost:8080'
# 'https://us-central1-majestic-disk-281618.cloudfunctions.net/batch'
resp = requests.post('http://localhost:8081/batch', \
                     json={'batch': data, 'user': 9})
print(resp)
print(resp.text)
