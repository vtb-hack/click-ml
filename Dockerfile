FROM python:3.7-slim

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY *.py /app/
COPY run.sh /app/
COPY model.pkl /app/

CMD cd /app && bash run.sh
