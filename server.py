from flask import Flask
from flask import request
from flask_cors import CORS
from helpers import *
app = Flask(__name__)
CORS(app)

@app.route('/batch', methods=['POST'])
def web_process_batch():
    return process_batch(request.json['batch'], request.json.get('user', None))

@app.route('/', methods=['POST'])
def web_register_event():
    data = request.json
    with open('data/myevents.txt', 'a') as file:
        print(data, file=file)
    return 'ok'
