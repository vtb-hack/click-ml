import settings
import csv
import os
import random
random.seed(0)

class LegacyDataset:
    def __init__(self, myid, is_train=True):
        labels = os.path.join(settings.BASE_FOLDER, settings.PUBLIC_LABELS)
        dict2ok = {}
        with open(labels) as file:
            reader = csv.DictReader(file)
            for row in reader:
                dict2ok[row['filename']] = 1 - int(row['is_illegal'])

        datafolder = settings.TRAINING_FOLDER if is_train else settings.TEST_FOLDER
        datafolder = os.path.join(settings.BASE_FOLDER, datafolder)
        positive = []
        negative = []
        for user in os.listdir(datafolder):
            print(int(user[4:]), 'user', int(user[4:]) == myid)
            for session in os.listdir(os.path.join(datafolder, user)):
                is_good = True
                if int(user[4:]) != myid:
                    is_good = False

                if session in dict2ok and not dict2ok[session]:
                    is_good = False
                elif session not in dict2ok and not is_train:
                    continue

                line = f'python3 rawdata2actions.py {os.path.join(datafolder, user, session)} data/actions.csv 2>/dev/null'
                os.system(line)
                results = []
                with open('data/actions.csv') as csvfile:
                    first = True
                    reader = csv.reader(csvfile)
                    for el in reader:
                        if first:
                            first = False
                            continue
                        results.append(map(float, el))
                if is_good:
                    positive += results
                else:
                    negative += results
        random.shuffle(positive)
        random.shuffle(negative)
        length = min(len(positive), len(negative))
        positive = positive[:length]
        negative = negative[:length]
        self.data = [list(el) + [1] for el in positive]
        self.data += [list(el) + [0] for el in negative]

    def tolist(self):
        return self.data

class LegacySessionDataset:
    def __init__(self, myid):
        is_train = False
        labels = os.path.join(settings.BASE_FOLDER, settings.PUBLIC_LABELS)
        dict2ok = {}
        with open(labels) as file:
            reader = csv.DictReader(file)
            for row in reader:
                dict2ok[row['filename']] = 1 - int(row['is_illegal'])

        datafolder = settings.TRAINING_FOLDER if is_train else settings.TEST_FOLDER
        datafolder = os.path.join(settings.BASE_FOLDER, datafolder)
        positive = []
        negative = []
        for user in os.listdir(datafolder):
            print(int(user[4:]), 'user', int(user[4:]) == myid)
            for session in os.listdir(os.path.join(datafolder, user)):
                is_good = True
                if int(user[4:]) != myid:
                    break

                if session in dict2ok and not dict2ok[session]:
                    is_good = False
                elif session not in dict2ok and not is_train:
                    continue

                line = f'python3 rawdata2actions.py {os.path.join(datafolder, user, session)} data/actions.csv 2>/dev/null'
                os.system(line)
                results = []
                with open('data/actions.csv') as csvfile:
                    first = True
                    reader = csv.reader(csvfile)
                    for el in reader:
                        if first:
                            first = False
                            continue
                        results.append(list(map(float, el)))
                if is_good:
                    positive.append(results)
                else:
                    negative.append(results)
        self.data = [[list(el), 1] for el in positive]
        self.data += [[list(el), 0] for el in negative]

    def tolist(self):
        return self.data

if __name__ == "__main__":
    print(len(LegacyDataset(7).tolist()))
    print(len(LegacySessionDataset(7).tolist()))
